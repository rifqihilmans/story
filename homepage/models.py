from django.db import models
from datetime import datetime

# Create your models here.
class jadwalKegiatan(models.Model):
    nama = models.CharField(max_length = 100, default = '')
    tempat = models.CharField(max_length = 100, default = '')
    kategori = models.CharField(max_length = 100, default = '')
    hari = models.DateTimeField(default=datetime.now)
    jam = models.TimeField(default = datetime.now)

def __str__(self):
    return "{}.{}".format(self.id, self.nama)