from django import forms
from .models import jadwalKegiatan

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = jadwalKegiatan
        fields = ['nama', 'tempat', 'kategori', 'hari', 'jam']
        widgets = {
            'nama': forms.TextInput(attrs = {'class': 'form-control'}),
            'tempat': forms.TextInput(attrs = {'class': 'form-control'}),
            'kategori': forms.TextInput(attrs = {'class': 'form-control'}),
            'hari': forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
            'jam': forms.TimeInput(attrs={'type': 'time', 'class': 'form-control' })
        }
        labels = {
            'nama' : 'Nama Kegiatan',
        }
    
