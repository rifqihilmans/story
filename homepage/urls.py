from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('experiences/', views.experiences, name='experiences'),
    path('skills/', views.skills, name='skills'),
    path('contacts/', views.contacts, name='contacts'),
    path('gallery/', views.gallery, name='gallery'),
    path('schedules/', views.schedules, name= 'schedules'),
    path('create/', views.create, name= 'create'),
    url(r'^delete/(?P<delete_id>[0-9]+)$', views.delete, name= 'delete'),
]
