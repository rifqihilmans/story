from django.shortcuts import render, redirect
from .models import jadwalKegiatan
from . import forms

# Create your views here.

def index(request):
    return render(request, 'index.html')
def experiences(request):
    return render(request, 'experiences.html')
def skills(request):
    return render(request, 'skills.html')
def contacts(request):
    return render(request, 'contacts.html')
def gallery(request):
    return render(request, 'gallery.html')
def schedules(request):
    semua_jadwal = jadwalKegiatan.objects.all()

    context = {
        'semua_jadwal': semua_jadwal,
    }
    return render(request, 'schedules.html', context)

def create(request):
    if request.method == 'POST':
        jadwal_form = forms.ScheduleForm(request.POST)
        if jadwal_form.is_valid():
            jadwal_form.save()
            return redirect('homepage:schedules')
    jadwal_form = forms.ScheduleForm()
    context = {
        'jadwal_form': jadwal_form,
    }
    return render(request, 'create.html', context)

def delete(request, delete_id):
    jadwalKegiatan.objects.filter(id=delete_id).delete()
    return redirect('homepage:schedules')
